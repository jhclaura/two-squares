import { Clock } from 'three'
import './style.css'
import Stats from 'stats.js'

import Game from './components/Game'

let game, clock, stats

window.onload = () => {
  start()
}

function start() {
  // stats = new Stats()
  // document.body.appendChild(stats.dom)

  clock = new Clock()

  game = new Game()
  game.init()

  window.addEventListener('resize', onWindowResize, false)

  animate()
}

function animate(t) {
  requestAnimationFrame(animate)

  let delta = clock.getDelta()
  game.animate(delta)
  // stats.update()
}

function onWindowResize() {
  game.onWindowResize()
}
