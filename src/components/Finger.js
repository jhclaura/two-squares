import * as THREE from 'three'
export default class Finger extends THREE.Object3D {
  constructor(fingerId, scene) {
    super()
    this.fingerId = fingerId
    this.scene = scene
    this.grabbingObject = undefined
    this.grabbingPoint = new THREE.Vector3()
    this.grabbingOffset = new THREE.Vector3()
    this.init()
  }

  init() {
    this.scene.add(this)
  }

  dispose() {
    this.scene.remove(this)
  }

  grab(object, point) {
    this.grabbingPoint.copy(point)
    this.grabbingObject = object
    this.grabbingObject.grabbedBy(this)
  }

  release() {
    this.grabbingPoint.set(0, 0, 0)
    this.grabbingObject.releasedBy(this)
    this.grabbingObject = undefined
  }

  updatePoint(point) {
    this.grabbingPoint.copy(point)
  }
}
