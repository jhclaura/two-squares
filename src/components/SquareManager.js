import * as THREE from 'three'

import Square from './Square'

export default class SquareManager extends THREE.Object3D {
  constructor(scene) {
    super()
    this.scene = scene
    this.squares = []
    this.tmpVertex = new THREE.Vector3()
    this.overlapCount = 0

    this.init()
  }

  init() {
    // info
    this.info = document.createElement('div')
    this.info.id = 'info'
    document.body.appendChild(this.info)
    this.info.innerHTML = 'Are they overlapped?'

    this.squareGeometry = new THREE.PlaneGeometry(1, 1)
    this.squareA = new Square(this.squareGeometry, 0xb28463, 'brownSQ')
    this.squareA.position.x += 5
    this.add(this.squareA)
    this.squares.push(this.squareA)

    this.squareB = new Square(this.squareGeometry, 0x698b9f, 'blueSQ')
    this.squareB.position.x -= 5
    this.add(this.squareB)
    this.squares.push(this.squareB)

    // For manually detecting intersect
    this.squareAVertices = {}
    this.squareBVertices = {}
    for (let i = 0; i < 4; i++) {
      let v = new THREE.Vector3()
      this.squareAVertices[i] = v
      v = new THREE.Vector3()
      this.squareBVertices[i] = v
    }

    //v.2
    this.squareALines = []
    this.squareBLines = []
    for (let i = 0; i < 4; i++) {
      let v = new THREE.Vector3()
      this.squareALines.push([v, v.clone()])
      this.squareBLines.push([v.clone(), v.clone()])
    }

    this.squareA.verticesWorldPosition = this.squareAVertices
    this.squareB.verticesWorldPosition = this.squareBVertices
    this.squareA.linesWorldPosition = this.squareALines
    this.squareB.linesWorldPosition = this.squareBLines
  }

  update() {
    this.squareA.update()
    this.squareB.update()
  }

  grabReport(square) {
    if (square === this.squareA) {
      this.squareA.changeRenderOrder(1)
      this.squareB.changeRenderOrder(0)
    } else {
      this.squareA.changeRenderOrder(0)
      this.squareB.changeRenderOrder(1)
    }
  }

  reportOverlap(overlapped) {
    overlapped === true ? this.overlapCount++ : this.overlapCount--

    if (this.overlapCount === 0) {
      this.info.style.backgroundColor = 'transparent'
      this.info.style.color = 'black'
      this.info.innerHTML = 'Are they overlapped?'
    } else {
      this.info.style.backgroundColor = 'tomato'
      this.info.style.color = 'white'
      this.info.innerHTML = 'Overlapped!'
    }
  }

  doOverlapChecking(square) {
    let anotherSquare = square === this.squareA ? this.squareB : this.squareA
    this.updateSquareVerticesPosition(square)
    this.updateSquareVerticesPosition(anotherSquare)

    this.checkIfSquareOverlapOther(square, anotherSquare)
  }

  checkIfPositionWithinSqaure(position, square, axis) {
    let squareVertexIndex = axis === 'x' ? 1 : 2
    let check =
      position[axis] > square.verticesWorldPosition[0][axis] &&
      position[axis] < square.verticesWorldPosition[squareVertexIndex][axis]
    return check
  }

  checkIfSquareOverlapOther(square, anotherSquare) {
    for (let i = 0; i < 4; i++) {
      let check1 = this.checkIfPositionWithinSqaure(
        square.verticesWorldPosition[i],
        anotherSquare,
        'x',
      )
      let check2 = this.checkIfPositionWithinSqaure(
        square.verticesWorldPosition[i],
        anotherSquare,
        'z',
      )
      if (check1 && check2) {
        square.overlapState = true
        return
      }
    }
    // check for cross
    let isWidthWithin =
      this.checkIfPositionWithinSqaure(
        square.verticesWorldPosition[0],
        anotherSquare,
        'x',
      ) &&
      this.checkIfPositionWithinSqaure(
        square.verticesWorldPosition[1],
        anotherSquare,
        'x',
      )

    let isHeightWithin =
      this.checkIfPositionWithinSqaure(
        square.verticesWorldPosition[0],
        anotherSquare,
        'z',
      ) &&
      this.checkIfPositionWithinSqaure(
        square.verticesWorldPosition[2],
        anotherSquare,
        'z',
      )

    let isWidthWithinB =
      this.checkIfPositionWithinSqaure(
        anotherSquare.verticesWorldPosition[0],
        square,
        'x',
      ) &&
      this.checkIfPositionWithinSqaure(
        anotherSquare.verticesWorldPosition[1],
        square,
        'x',
      )

    let isHeightWithinB =
      this.checkIfPositionWithinSqaure(
        anotherSquare.verticesWorldPosition[0],
        square,
        'z',
      ) &&
      this.checkIfPositionWithinSqaure(
        anotherSquare.verticesWorldPosition[2],
        square,
        'z',
      )

    square.overlapState =
      isWidthWithin !== isHeightWithin && isWidthWithinB !== isHeightWithinB

    if (square.overlapState) return

    // Auto check another if it is idle
    if (!anotherSquare.beGrabbed) {
      this.checkIfSquareOverlapOther(anotherSquare, square)
    }
  }

  updateSquareVerticesPosition(square) {
    for (let i = 0; i < 4; i++) {
      this.tmpVertex.copy(this.squareGeometry.vertices[i])
      square.shape.localToWorld(this.tmpVertex)
      square.verticesWorldPosition[i].copy(this.tmpVertex)
    }
  }
}
