import * as THREE from 'three'
import EventBus from './EventBus'
import Finger from './Finger'

export default class InteractionManager {
  constructor(camera, scene, base, squareManager) {
    this.camera = camera
    this.scene = scene
    this.base = base
    this.squareManager = squareManager

    // {fingerId:number, finger:Finger}
    this.fingers = {}
    this.raycaster = new THREE.Raycaster()
    this.finger = new THREE.Vector2()

    this.init()
  }

  init() {
    window.addEventListener('touchmove', this.onTouchMove, { passive: false })
    window.addEventListener('touchstart', this.onTouchStart, false)
    window.addEventListener('touchend', this.onTouchEnd, false)

    window.addEventListener('mousemove', this.onMouseMove, false)
    window.addEventListener('mousedown', this.onMouseDown, false)
    window.addEventListener('mouseup', this.onMouseUp, false)
  }

  raycast(target = this.scene.children) {
    this.raycaster.setFromCamera(this.finger, this.camera)
    let intersects
    if (target.length > 1)
      intersects = this.raycaster.intersectObjects(target, true)
    else {
      intersects = this.raycaster.intersectObject(target, true)
    }
    if (intersects.length > 0) {
      return intersects[0]
    } else {
      return undefined
    }
  }

  fingerDown(fingerId) {
    const hit = this.raycast(this.squareManager.squares)
    if (!hit) return
    if (hit.object === this.base) return
    if (fingerId in this.fingers) return

    let newFinger = new Finger(fingerId, this.scene)
    newFinger.grab(hit.object.owner, hit.point)
    this.fingers[fingerId] = newFinger
  }

  fingerUp(fingerId) {
    if (!(fingerId in this.fingers)) return

    this.fingers[fingerId].release()
    this.fingers[fingerId].dispose()
    delete this.fingers[fingerId]
  }

  ////////////////////////
  ////////////////////////
  ////////////////////////

  onTouchStart = (e) => {
    e.preventDefault()
    let touches = e.changedTouches
    for (let i = 0; i < touches.length; i++) {
      this.updateFingerPosition(touches[i].pageX, touches[i].pageY)
      this.fingerDown(touches[i].identifier)
    }
  }

  onTouchEnd = (e) => {
    e.preventDefault()
    let touches = e.changedTouches
    for (let i = 0; i < touches.length; i++) {
      this.fingerUp(touches[i].identifier)
    }
  }

  onTouchMove = (e) => {
    e.preventDefault()
    let touches = e.changedTouches

    for (let i = 0; i < touches.length; i++) {
      if (touches[i].identifier in this.fingers) {
        this.updateFingerPosition(touches[i].pageX, touches[i].pageY)
        const hit = this.raycast(this.base)
        if (!hit) return
        this.fingers[touches[i].identifier].updatePoint(hit.point)
      }
    }
  }

  ////////////////////////
  ////////////////////////
  ////////////////////////

  onMouseDown = (e) => {
    this.fingerDown(e.button)
  }

  onMouseUp = (e) => {
    this.fingerUp(e.button)
  }

  onMouseMove = (e) => {
    this.updateFingerPosition(e.clientX, e.clientY)

    if (e.button in this.fingers) {
      const hit = this.raycast(this.base)
      if (!hit) return
      this.fingers[e.button].updatePoint(hit.point)
    }
  }

  ////////////////////////
  ////////////////////////
  ////////////////////////

  updateFingerPosition(x, y) {
    this.finger.x = (x / window.innerWidth) * 2 - 1
    this.finger.y = -(y / window.innerHeight) * 2 + 1
  }
}
