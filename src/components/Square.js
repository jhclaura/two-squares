import * as THREE from 'three'

export default class Square extends THREE.Object3D {
  constructor(geometry, color, name) {
    super()
    this.geometry = geometry
    this.color = color
    this.name = name

    this.beGrabbed = false
    this.beStretched = false
    this.grabFinger = undefined
    this.grabOffset = new THREE.Vector3()

    this.stretchFinger = undefined
    this.fingersCenter = new THREE.Vector3()
    this.fingersCenterOffset = new THREE.Vector3()
    this.fingersInitialOffset = new THREE.Vector3()
    this.fingersScaleInitialOffset = new THREE.Vector3()
    this.scaleBeforeStretch = new THREE.Vector3()

    this.extraHeight = 0

    this.raycaster = new THREE.Raycaster()
    this.upDirection = new THREE.Vector3(0, 1, 0)
    this.downDirection = new THREE.Vector3(0, -1, 0)
    this.tmpVertex = new THREE.Vector3()

    this._overlapState = false

    this.init()
  }

  get overlapState() {
    return this._overlapState
  }

  set overlapState(state) {
    if (state !== this._overlapState) this.parent.reportOverlap(state)
    this._overlapState = state
  }

  init() {
    this.material = new THREE.MeshBasicMaterial({
      color: this.color,
    })

    this.shape = new THREE.Mesh(this.geometry, this.material)
    this.shape.rotateX(-Math.PI / 2)
    this.shape.owner = this

    this.add(this.shape)
    this.scale.set(6, 1, 6)
  }

  changeRenderOrder(order) {
    this.extraHeight = order * 0.1
    this.shape.position.y = this.extraHeight
  }

  grabbedBy(finger) {
    if (this.grabFinger === undefined) {
      this.beGrabbed = true
      this.parent.grabReport(this)
      this.grabFinger = finger

      this.grabOffset.subVectors(this.position, this.grabFinger.grabbingPoint)
      this.grabOffset.y = this.position.y
    } else if (this.stretchFinger === undefined) {
      this.beStretched = true
      this.stretchFinger = finger

      this.fingersCenter
        .addVectors(
          this.grabFinger.grabbingPoint,
          this.stretchFinger.grabbingPoint,
        )
        .divideScalar(2)
      this.fingersCenter.y = this.position.y
      this.fingersCenterOffset.subVectors(this.position, this.fingersCenter)

      this.fingersInitialOffset.subVectors(
        this.stretchFinger.grabbingPoint,
        this.grabFinger.grabbingPoint,
      )
      this.fingersInitialOffset.y = this.position.y
      this.fingersInitialOffset.x = Math.abs(this.fingersInitialOffset.x)
      this.fingersInitialOffset.z = Math.abs(this.fingersInitialOffset.z)

      this.fingersScaleInitialOffset.subVectors(
        this.scale,
        this.fingersInitialOffset,
      )

      this.scaleBeforeStretch.copy(this.scale)
    }
  }

  releasedBy(finger) {
    if (this.grabFinger === finger) {
      if (this.stretchFinger !== undefined) {
        this.grabFinger = this.stretchFinger
        this.grabOffset.subVectors(this.position, this.grabFinger.grabbingPoint)
        this.grabOffset.y = this.position.y

        this.beStretched = false
        this.stretchFinger = undefined
      } else {
        this.beGrabbed = false
        this.grabFinger = undefined
      }
    } else if (this.stretchFinger === finger) {
      this.beStretched = false
      this.stretchFinger = undefined
    }
  }

  update() {
    if (this.stretchFinger) {
      // size
      let currentFingersOffset = new THREE.Vector3()
      currentFingersOffset.subVectors(
        this.stretchFinger.grabbingPoint,
        this.grabFinger.grabbingPoint,
      )
      currentFingersOffset.y = this.position.y
      currentFingersOffset.y = 0
      currentFingersOffset.x = Math.abs(currentFingersOffset.x)
      currentFingersOffset.z = Math.abs(currentFingersOffset.z)

      this.scale.addVectors(
        currentFingersOffset,
        this.fingersScaleInitialOffset,
      )

      // position
      this.fingersCenter
        .addVectors(
          this.grabFinger.grabbingPoint,
          this.stretchFinger.grabbingPoint,
        )
        .divideScalar(2)
      this.fingersCenter.y = this.position.y
      this.position.addVectors(this.fingersCenter, this.fingersCenterOffset)

      // update
      this.grabOffset.subVectors(this.position, this.grabFinger.grabbingPoint)
      this.grabOffset.y = this.position.y

      //this.checkIfOverlay()
      this.parent.doOverlapChecking(this)
    } else if (this.beGrabbed) {
      let newPos = new THREE.Vector3().addVectors(
        this.grabFinger.grabbingPoint,
        this.grabOffset,
      )
      newPos.y = this.position.y
      this.position.copy(newPos)

      //this.checkIfOverlay()
      this.parent.doOverlapChecking(this)
    }
  }

  // Raycast version, but refrained from using it based on the instruction
  checkIfOverlay() {
    for (let i = 0; i < this.geometry.vertices.length; i++) {
      this.tmpVertex.copy(this.geometry.vertices[i])
      this.shape.localToWorld(this.tmpVertex)
      this.tmpVertex.y += 10
      let possibleIntersect = this.overlayRaycast(
        this.tmpVertex,
        this.downDirection,
      )
      if (possibleIntersect) {
        this.overlapState = true
        return
      }
    }
    this.overlapState = false
  }

  overlayRaycast(origin, direction) {
    this.raycaster.set(origin, direction)
    let targetsArray = this.parent.squares.filter((s) => s !== this)
    let intersects = this.raycaster.intersectObjects(targetsArray, true)
    if (intersects.length > 0) {
      return intersects[0]
    } else {
      return undefined
    }
  }
}
