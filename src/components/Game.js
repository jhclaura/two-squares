import * as THREE from 'three'

import InteractionManager from './InteractionManager'
import SquareManager from './SquareManager'

export default class Game {
  constructor() {}

  init() {
    this.container = document.createElement('div')
    document.body.appendChild(this.container)

    this.scene = new THREE.Scene()
    this.scene.background = new THREE.Color(0xe3d2b8)

    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000,
    )
    this.camera.position.y = 20
    this.camera.lookAt(0, 0, 0)
    this.scene.add(this.camera)

    this.base = new THREE.Mesh(
      new THREE.BoxGeometry(100, 1, 100),
      new THREE.MeshBasicMaterial({ color: 0xe3d2b8, visible: false }),
    )
    this.base.name = 'base'
    this.base.position.y -= 0.5
    this.scene.add(this.base)

    this.squareManager = new SquareManager()
    this.scene.add(this.squareManager)

    this.interactionManager = new InteractionManager(
      this.camera,
      this.scene,
      this.base,
      this.squareManager,
    )

    this.renderer = new THREE.WebGLRenderer({ antialias: true })
    // this.renderer.setPixelRatio(window.devicePixelRatio)
    this.renderer.setSize(window.innerWidth, window.innerHeight)
    this.container.appendChild(this.renderer.domElement)
  }

  animate(delta) {
    this.squareManager.update()

    this.render()
  }

  render() {
    this.renderer.render(this.scene, this.camera)
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight
    this.camera.updateProjectionMatrix()
    this.renderer.setSize(window.innerWidth, window.innerHeight)
  }
}
